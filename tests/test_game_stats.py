from preggy import expect

from game_stats.controller import Controller
from game_stats.statistics import GameStats
from tests.base import BaseTestCase
from tests import testutils
from tests.rng import rng


class GameStatsTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.controller = Controller()
        self.game_stats = GameStats()

    def test_game_stats(self):
        # given
        panel, _ = self._create_game_result()

        # when
        stats = self.game_stats.game_stats(panel)

        # then
        for game in stats:
            expect(game['count']).to_be_greater_than(0)
            expect(len(game['highscores'])).to_be_greater_than(0)
            expect(len(game['lowscores'])).to_be_greater_than(0)

    def test_games_count(self):
        # given
        league_id = 'test_league'
        self._create_game_result(
            league_id=league_id,
            players_per_game=4,
            num_results=5
        )

        panel, _ = self._create_game_result(
            league_id=league_id,
            players_per_game=5,
            num_results=5,
            result_index=100
        )

        # when
        counts = self.game_stats.games_count(panel)

        # then
        expect(counts.values.sum()).to_equal(10)
        expect(counts.values).Not.to_include(0)

    def test_get_highscores(self):
        # given
        league_id = 'test_league'
        self._create_game_result(
            league_id=league_id,
            players_per_game=4,
            num_results=5
        )
        panel, result = self._create_game_result(
            league_id=league_id,
            players_per_game=5,
            num_results=5,
            result_index=100
        )

        # create a new game result with a highscore of 1000
        new_scores = [
            ('player-1', rng.randint(0, 100)),
            ('player-2', 1000),
            ('player-3', 1000),
            ('player-4', rng.randint(0, 100)),
        ]

        testutils.create_fake_game_result(
            self.controller,
            'test',
            league_id=league_id,
            players_per_game=5,
            game_id=result.game_id,
            scores=new_scores
        )

        panel = self.controller.get_scores_panel(league_id)

        # when
        highscores = self.game_stats.highscores(panel)

        # then
        expect(highscores[result.game_id]).to_include('player-2')
        expect(highscores[result.game_id]).to_include('player-3')

        # every game should have at least 1 highscore
        for game in highscores.keys():
            expect(len(highscores[game])).to_be_greater_than(0)


    def test_get_lowscores(self):
        # given
        league_id = 'test_league'
        self._create_game_result(
            league_id=league_id,
            players_per_game=4,
            num_results=5
        )
        panel, result = self._create_game_result(
            league_id=league_id,
            players_per_game=5,
            num_results=5,
            result_index=100
        )

        # create a new game result with a highscore of 1000
        new_scores = [
            ('player-1', rng.randint(0, 100)),
            ('player-2', -1000),
            ('player-3', -1000),
            ('player-4', rng.randint(0, 100)),
        ]

        testutils.create_fake_game_result(
            self.controller,
            'test',
            league_id=league_id,
            players_per_game=5,
            game_id=result.game_id,
            scores=new_scores
        )

        panel = self.controller.get_scores_panel(league_id)

        # when
        lowscores = self.game_stats.lowscores(panel)

        # then
        expect(lowscores[result.game_id]).to_include('player-2')
        expect(lowscores[result.game_id]).to_include('player-3')

        # every game should have at least 1 highscore
        for game in lowscores.keys():
            expect(len(lowscores[game])).to_be_greater_than(0)

    def test_get_game_means_many(self):
        # given
        panel, result = self._create_game_result()

        # when
        game_means = self.game_stats.get_game_means(panel, result.game_id)

        # then
        expect(game_means[0][1]).to_be_greater_than(0)
        expect([x for x in game_means if x[0] == 4]).to_length(1)

    def test_get_game_means_single(self):
        # given
        panel, result = self._create_game_result(num_results=1)
        mean = sum([x[1] for x in result.scores]) / len(result.scores)

        # when
        game_means = self.game_stats.get_game_means(panel, result.game_id)

        # then
        expect(game_means[0][1]).to_equal(mean)

    def test_get_mean_single_num_players(self):
        # given
        panel, result = self._create_game_result(
            players_per_game=4
        )

        # when
        all_means = self.game_stats.get_game_means(panel, result.game_id)
        single_mean = self.game_stats.get_game_means(panel, result.game_id, num_players=4)

        # then
        expect(all_means[0][1]).to_equal(single_mean)

    def test_get_game_mean_single_game(self):
        # given
        panel, result = self._create_game_result(num_results=1)

        num_players = len(result.scores)
        mean = sum([x[1] for x in result.scores]) / num_players

        # when
        game_mean = self.game_stats.compute_mean_single_game(panel, result.game_id, num_players)

        # then
        expect(game_mean).to_equal(mean)

    def test_get_game_highscores(self):
        # given
        players_per_game = 4
        league_id = 'test_league'
        panel, result = self._create_game_result(
            max_score=100,
            players_per_game=players_per_game,
            league_id=league_id
        )

        # create a new game result with a highscore of 1000
        new_scores = [
            ('player-1', rng.randint(0, 100)),
            ('player-2', 1000),
            ('player-3', 1000),
            ('player-4', rng.randint(0, 100)),
        ]

        new_result = testutils.create_fake_game_result(
            self.controller,
            'test',
            league_id=league_id,
            game_id=result.game_id,
            scores=new_scores
        )

        panel = self.controller.get_scores_panel(league_id)

        # when
        highscores = self.game_stats.get_game_highscores(panel, new_result.game_id)

        # then
        expect(highscores[0][0]).to_equal(4) # num players
        expect(highscores[0][1][1]).to_include('player-2') # player
        expect(highscores[0][1][1]).to_include('player-3') # player
        expect(highscores[0][1][0]).to_equal(1000) # high score

    def test_get_single_highscore(self):
        # given
        panel, result = self._create_game_result(
            players_per_game=4
        )

        # when
        all_highscores = self.game_stats.get_game_highscores(panel, result.game_id)
        single_highscore = self.game_stats.get_game_highscores(
            panel,
            result.game_id,
            num_players=4
        )

        # then
        expect(all_highscores[0][1]).to_equal(single_highscore)

    def test_get_lowscores_2(self):
        # given
        players_per_game = 4
        league_id = 'test_league'
        panel, result = self._create_game_result(
            max_score=100,
            players_per_game=players_per_game,
            league_id=league_id
        )

        # create a new game result with a highscore of 1000
        new_scores = [
            ('player-1', rng.randint(0, 100)),
            ('player-2', -1000),
            ('player-3', -1000),
            ('player-4', rng.randint(0, 100)),
        ]

        new_result = testutils.create_fake_game_result(
            self.controller,
            'test',
            league_id=league_id,
            game_id=result.game_id,
            scores=new_scores
        )

        panel = self.controller.get_scores_panel(league_id)

        # when
        highscores = self.game_stats.get_game_lowscores(panel, new_result.game_id)

        # then
        expect(highscores[0][0]).to_equal(4) # num players
        expect(highscores[0][1][1]).to_include('player-2') # player
        expect(highscores[0][1][1]).to_include('player-3') # player
        expect(highscores[0][1][0]).to_equal(-1000) # high score

    # helpers

    def _create_game_result(
            self,
            num_results=10,
            num_games=5,
            max_score=100,
            players_per_game=4,
            league_id='test_league',
            result_index=0,
    ):
        fake_result_factory = testutils.FakeResultFactory(
            self.controller,
            num_players=5,
            num_games=num_games,
            max_score=max_score,
            players_per_game=players_per_game,
            league_id=league_id,
            result_index=result_index
        )
        results = fake_result_factory.create(num_results)

        panel = self.controller.get_scores_panel(league_id)

        return panel, results[rng.randint(0, num_results - 1)]