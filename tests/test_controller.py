from preggy import expect
from datetime import datetime
import numpy as np

from game_stats.controller import Controller
from tests.base import BaseTestCase
from tests import testutils
from tests.testutils import FakeResultFactory

class ControllerTestCase(BaseTestCase):

    def setUp(self):
        super().setUp()
        self.controller = Controller()

    def test_save_game_result(self):
        self.controller.set_game_weight('test-game', 7)
        self.controller.save_game_result(
            datetime(2015, 1, 1),
            'test_league',
            'test-game',
            'test-result',
            [
                ('player1', 10),
                ('player2', 11),
                ('player3', 12),
            ]
        )

        panel = self.controller.get_scores_panel('test_league')

        games = list(panel.index.get_level_values('games').drop_duplicates())
        players = list(panel.index.get_level_values('players').drop_duplicates())
        results = list(panel.index.get_level_values('results').drop_duplicates())

        # check the axis lengths of the resulting league panel
        expect(games).to_length(1) # one game
        expect(players).to_length(3) # three players
        expect(results).to_length(1) # one result

        # check players
        expect(players).to_include('player1')
        expect(players).to_include('player2')
        expect(players).to_include('player3')

        # check scores
        expect(panel[:, :, 'player1'][0]).to_equal(10)
        expect(panel[:, :, 'player2'][0]).to_equal(11)
        expect(panel[:, :, 'player3'][0]).to_equal(12)

        # check metadata
        created = self.controller.get_metadata('test_league', result_id='test-result', key='created')
        expect(created.to_pydatetime()).to_equal(datetime(2015, 1, 1))

    def test_update_game_result(self):
        # test that when saving a result with an existing id, the scores panel is updated
        # correctly, overwriting the existing result with the new one.

        # given
        league_id = 'league_id'
        result = testutils.create_fake_game_result(self.controller, 'test', league_id=league_id)
        panel = self.controller.get_scores_panel(league_id)

        # when
        testutils.create_fake_game_result(
            self.controller,
            'test',
            scores=list(map(lambda x: (x[0], x[1] + 1), result.scores)),
            league_id=league_id
        )

        # then
        new_panel = self.controller.get_scores_panel(league_id)

        expect(new_panel.index.equals(panel.index))
        expect(np.array_equal(panel.values, new_panel.values)).to_be_false()
        expect(np.array_equal(
            panel.values - new_panel.values, [-1, -1, -1, -1])
        ).to_be_true()

    def test_update_game_weight(self):
        # given
        num_games = 5
        new_weight = 3
        results = testutils.create_fake_game_results(self.controller, num_games, 4, 3)

        # when
        self.controller.set_game_weight(results[0].game_id, new_weight)

        # then
        weights = self.controller.get_game_weights()
        expect(weights[results[0].game_id]).to_equal(new_weight)

    def test_get_game_weights(self):
        # given
        weight = 7
        game_id = 'test_game'
        self.controller.set_game_weight(game_id, weight)

        # when
        weight_loaded = self.controller.get_game_weights()[game_id]

        # then
        expect(weight_loaded).to_equal(weight)

    def test_get_specific_game_weight(self):
        # given
        weight = 7
        game_id = 'test_game'
        self.controller.set_game_weight(game_id, weight)

        # when
        loaded_weight = self.controller.get_game_weights(game_id=game_id)

        # then
        expect(loaded_weight).to_equal(weight)

    def test_get_metadata(self):
        # given
        self.controller.set_game_weight('game-id', 3)
        self.controller.save_game_result(
            datetime(2015, 1, 1),
            'test_league',
            'game-id',
            'test-result',
            [
                ('player1', 10),
                ('player2', 11),
                ('player3', 12),
            ]
        )

        meta = self.controller.get_metadata('test_league')

    def test_get_leagues(self):
        # given
        league_id = 'test_league'
        testutils.create_fake_game_results(self.controller, 1, 4, 3, league_id=league_id)

        # when
        leagues = self.controller.get_leagues()

        # then
        expect(leagues).to_equal([league_id])

    def test_delete_gameresult(self):
        # given
        league_id = 'test_league'
        results = testutils.create_fake_game_results(self.controller, 3, 3, 3, league_id=league_id)
        result_to_delete = results[7].result_id

        # when
        timeseries_before = self.controller.get_timeseries(league_id)
        self.controller.delete_gameresult(league_id, result_to_delete)
        timeseries_after = self.controller.get_timeseries(league_id)

        # then
        expect(timeseries_before.size).to_be_greater_than(timeseries_after.size)
        expect(timeseries_after.columns).Not.to_include(result_to_delete)

    def test_delete_gameresult_and_then_add_gameresult(self):
        # given
        league_id = 'test_league'
        result_factory = FakeResultFactory(
            self.controller,
            league_id=league_id,
            num_players=4,
            players_per_game=4,
        )
        results = result_factory.create(3)
        result_to_delete = results[1].result_id
        timeseries_before = self.controller.get_timeseries(league_id)

        # when
        self.controller.delete_gameresult(league_id, result_to_delete)
        result_factory.create(1)
        timeseries_after = self.controller.get_timeseries(league_id)

        # then
        expect(timeseries_before.size).to_equal(timeseries_after.size)
        expect(timeseries_after.columns).Not.to_include(result_to_delete)

    def test_delete_gameresult_and_then_add_same_gameresult(self):
        # given
        league_id = 'test_league'
        result_factory = FakeResultFactory(
            self.controller,
            league_id=league_id,
            num_players=4,
            players_per_game=4,
            num_games=2
        )
        results = result_factory.create(10)
        timeseries_before = self.controller.get_timeseries(league_id)
        result_to_delete = results[5]

        # when
        self.controller.delete_gameresult(league_id, result_to_delete.result_id)
        testutils.save_fake_game_result(self.controller, result_to_delete)

        timeseries_after = self.controller.get_timeseries(league_id)

        # then
        expect(timeseries_after.equals(timeseries_before)).to_equal(True)

    def test_delete_gameresult_single_result(self):
        # given
        league_id = 'test_league'
        result_factory = FakeResultFactory(self.controller, league_id=league_id)
        result_to_delete = result_factory.create(1)[0]

        # when
        self.controller.delete_gameresult(league_id, result_to_delete.result_id)
        timeseries_after = self.controller.get_timeseries(league_id)

        # then
        expect(timeseries_after.size).to_equal(0)

    def test_save_gameresult_out_of_order(self):
        # If a game result is saved out of order (not the newest one), the panel must be re-indexed
        # to correctly reflect the order in which the games occured.

        # given
        league_id = 'test_league'
        results = testutils.create_fake_game_results(self.controller, 3, 3, 3, league_id=league_id)

        # when
        self.controller.save_game_result(
            datetime(1970, 1, 1),
            league_id,
            results[0].game_id,
            'early_result',
            results[0].scores
        )

        # then
        panel = self.controller.get_scores_panel(league_id)
        expect(panel.index.get_level_values('results').unique()[0]).to_equal('early_result')

