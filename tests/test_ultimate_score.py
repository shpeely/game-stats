from preggy import expect
import pandas as pd

from game_stats.constants import RESULT_LEVEL, PLAYER_LEVEL, GAME_LEVEL
from game_stats.controller import Controller
from game_stats.statistics import UltimateScore
from tests.base import BaseTestCase
from tests import testutils


class UltimateScoreTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.controller = Controller()
        self.ultimate_score = UltimateScore()

    def test_recompute_ultimate_score_timeseries(self):
        # create fake games
        num_games = 4
        num_players = 3
        results_per_game = 2

        results = testutils.create_fake_game_results(
            self.controller,
            num_games,
            num_players,
            results_per_game
        )

        panel = self.controller.get_scores_panel('empty_league')
        timeseries = pd.DataFrame()
        metadata = pd.DataFrame()
        weights = self.controller.get_game_weights()

        for result in results:
            panel, timeseries, metadata = self.ultimate_score.update_ultimate_score_timeseries(
                panel,
                timeseries,
                metadata,
                weights,
                result.time,
                result.game_id,
                result.result_id,
                result.scores
            )

        result = self.ultimate_score.recompute_ultimate_score_time_series(panel, weights)

        timeseries = timeseries.sort_index()
        result = result.sort_index()

        expect(result.equals(timeseries)).to_be_true()

    def test_update_game_weight(self):
        # create fake games
        num_games = 4
        num_players = 3
        results_per_game = 2

        results = testutils.create_fake_game_results(
            self.controller,
            num_games,
            num_players,
            results_per_game
        )

        panel = self.controller.get_scores_panel('empty_league')
        timeseries = pd.DataFrame()
        metadata = pd.DataFrame()
        weights = self.controller.get_game_weights()

        for result in results:
            panel, timeseries, metadata = self.ultimate_score.update_ultimate_score_timeseries(
                panel,
                timeseries,
                metadata,
                weights,
                result.time,
                result.game_id,
                result.result_id,
                result.scores
            )

        new_weights = self.controller.get_game_weights() * 2
        result = self.ultimate_score.recompute_ultimate_score_time_series(panel, new_weights)

        timeseries = timeseries.sort_index()
        result = result.sort_index()

        expect((result).equals(timeseries * 2)).to_be_true()

    def test_update_ultimate_score_timeseries(self):
        # create fake games
        fake_result_factory = testutils.FakeResultFactory(self.controller)
        results = fake_result_factory.create(10)

        league_id = results[0].league_id
        panel_before = self.controller.get_scores_panel(league_id)
        timeseries_before = self.controller.get_timeseries(league_id)
        metadata = self.controller.get_metadata(league_id)

        result = fake_result_factory.create(1, update_hdf=False)[0]

        weights = self.controller.get_game_weights()
        panel, timeseries, metadata = self.ultimate_score.update_ultimate_score_timeseries(
            panel_before,
            timeseries_before,
            metadata,
            weights,
            result.time,
            result.game_id,
            result.result_id,
            result.scores
        )

        # timeseries should have one more result
        expect(len(timeseries.columns)).to_equal(len(timeseries_before.columns) + 1)

        # sum of all scores should still equal 0
        expect(timeseries.sum().sum()).to_be_lesser_or_equal_to(1e-10)

    def test_compute_single_game_result_many(self):
        # create fake games
        fake_result_factory = testutils.FakeResultFactory(self.controller, num_players=5, num_games=5)
        results = fake_result_factory.create(10)

        league_id = results[0].league_id
        panel = self.controller.get_scores_panel(league_id)
        weights = self.controller.get_game_weights()

        game_result = results[7]
        scores = self.ultimate_score.compute_single_game_result(panel, weights, game_result.result_id)

        expect(len(scores)).to_equal(len(game_result.scores))

        max_score = max(game_result.scores, key=lambda s: s[1])[1]
        winners = list(map(lambda s: s[0], filter(lambda s: s[1] == max_score, game_result.scores)))

        if len(winners) > 1:
            # if there is more than one winner we just check that when grouping the scores
            # by value, the count of the grouped scores should be less than the number of players.
            # Or in other words, at least 2 players had the same score.
            expect(scores.groupby(scores).count().size).to_be_lesser_than(len(game_result.scores))
        else:
            # Normal case: make sure that the winner of the game has gained most points
            expect(scores.idxmax()).to_equal(winners[0])

    def test_compute_single_game_result_single_game(self):
        # Test that when there is only a single result of a game, the sum of all point gained
        # should be zero (or near zero due to rounding errors)

        # given
        fake_result_factory = testutils.FakeResultFactory(self.controller, num_players=5, num_games=5)
        results = fake_result_factory.create(1)

        league_id = results[0].league_id
        game_result = results[0]
        panel = self.controller.get_scores_panel(league_id)
        weights = self.controller.get_game_weights()

        # when
        scores = self.ultimate_score.compute_single_game_result(panel, weights, game_result.result_id)

        # then
        expect(scores.sum()).to_be_lesser_or_equal_to(1e-10)

    def test_delete_gameresult(self):
        # given
        league_id = 'test_league'
        fake_result_factory = testutils.FakeResultFactory(self.controller, league_id=league_id)
        results = fake_result_factory.create(3)
        result_to_delete = results[2].result_id

        panel = self.controller.get_scores_panel(league_id)
        weights = self.controller.get_game_weights()

        # when
        new_panel = self.ultimate_score.delete_gameresult(panel, result_to_delete)

        # then
        remaining_result_ids = new_panel.index.get_level_values(RESULT_LEVEL).unique()
        expect(remaining_result_ids).Not.to_include(result_to_delete)
        expect(remaining_result_ids).to_length(2)

    def test_delete_gameresult_no_zombie_players(self):
        # given
        league_id = 'test_league'
        fake_result_factory = testutils.FakeResultFactory(self.controller, league_id=league_id)
        results = fake_result_factory.create(3)
        result_to_delete = testutils.create_fake_game_result(
            self.controller,
            '999',
            league_id='test_league',
            scores=[
                (results[0].scores[0][0], 100),
                (results[0].scores[1][0], 101),
                ('new-player', 200), # new player
            ]
        )

        panel = self.controller.get_scores_panel(league_id)

        # when
        new_panel = self.ultimate_score.delete_gameresult(panel, result_to_delete.result_id)

        # then
        expect(list(new_panel.index.get_level_values(PLAYER_LEVEL))).Not.to_include('new-player')

    def test_delete_gameresult_single_result(self):
        # given
        league_id = 'test_league'
        fake_result_factory = testutils.FakeResultFactory(self.controller, league_id=league_id)
        result_to_delete = fake_result_factory.create(1)[0]

        panel = self.controller.get_scores_panel(league_id)

        # when
        new_panel = self.ultimate_score.delete_gameresult(panel, result_to_delete.result_id)

        # then
        expect(list(new_panel.index.get_level_values(RESULT_LEVEL))).Not.to_include(result_to_delete.result_id)
