from game_stats.connection import Connection, Query, Mutation
from game_stats.adapters import Adapter
from tests.base import BaseTestCase
from unittest.mock import MagicMock

class ConnectionTestCase(BaseTestCase):

    def setUp(self):
        super().setUp()
        self.controller = MagicMock()
        self.adapter = Adapter()
        self.connection = Connection(self.controller, self.adapter)

    def test_get_meta(self):
        # given
        league_id = 'test_league'
        self.controller.get_metadata = MagicMock()

        # when
        self.connection.handle_request(Query.get_meta, league_id)

        # then
        self.controller.get_metadata.assert_called()

    def test_set_global_meta(self):
        # given
        meta = {'test': 123}
        self.controller.set_global_meta_multiple = MagicMock(return_value=meta)

        # when
        self.connection.set_global_meta(meta)

        # then
        self.controller.set_global_meta_multiple.assert_called_with(meta)

    def test_get_global_meta(self):
        # given
        self.controller.get_global_meta = MagicMock()

        # when
        self.connection.get_global_meta('key')

        # then
        self.controller.get_global_meta.assert_called_with('key')

    def test_get_leagues(self):
        # given
        self.controller.get_leagues = MagicMock()

        # when
        self.connection.get_leagues()

        # then
        self.controller.get_leagues.assert_called()

    def test_delete_gameresut(self):
        # given
        self.controller.delete_gameresult = MagicMock()
        data = { 'result_id': 'result-id', 'league_id': 'league-id' }

        # when
        self.connection.delete_gameresult(data)

        # then
        self.controller.delete_gameresult.assert_called_with(
            'league-id', 'result-id'
        )

    def test_get_gameresults(self):
        # given
        league_id = 'test_league'
        self.controller.get_gameresults = MagicMock()

        # when
        self.connection.get_gameresults(league_id)

        # then
        self.controller.get_scores_panel.assert_called_with(
            league_id
        )

    def test_get_game_result_stats(self):
        # given
        league_id = 'test_league'
        result_id = 'test-result'
        self.controller.get_game_result_stats = MagicMock()

        # when
        self.connection.get_game_result_stats(league_id, result_id)

        # then
        self.controller.get_game_result_stats.assert_called_with(
            league_id, result_id
        )

    def test_get_game_stats(self):
        # given
        league_id = 'test_league'
        self.controller.get_game_stats = MagicMock()

        # when
        self.connection.get_game_stats(league_id)

        # then
        self.controller.get_game_stats.assert_called_with(league_id)