from game_stats.store import StoreFactory
from tests.base import BaseTestCase
import pandas as pd
from preggy import expect


class FsStoreTestCase(BaseTestCase):

    def setUp(self):
        super().setUp()
        self.store = StoreFactory.get_store('fs')

    def test_save_and_load(self):
        # given
        filename = 'test.hd5'
        df = pd.DataFrame([1,2,3])

        # when
        self.store.save(filename, df)

        # then
        expect(self.store.load(filename).equals(df)).to_equal(True)

    def test_exists(self):
        # given
        filename = 'test.hd5'
        self.store.save(filename, pd.DataFrame())

        # then
        expect(self.store.exists(filename)).to_equal(True)

    def test_list(self):
        # given
        filenames = ['test' + str(i) + '.hd5' for i in range(3)]

        # when
        for name in filenames:
            self.store.save(name, pd.DataFrame())

        # then
        expect(set(filenames).issubset(self.store.list())).to_equal(True)