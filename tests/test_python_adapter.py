from preggy import expect
from game_stats.adapters import PythonAdapter
from game_stats.controller import Controller
from tests.base import BaseTestCase
from tests import testutils

class PythonAdapterTestCase(BaseTestCase):

    def setUp(self):
        super().setUp()
        self.controller = Controller()
        self.adapter = PythonAdapter()

    def test_get_gameresults_response(self):
        # given
        league_id = 'test_league'
        num_games = 3
        results_per_game = 3
        testutils.create_fake_game_results(
            self.controller,
            num_games,
            5,
            results_per_game,
            league_id=league_id
        )
        panel = self.controller.get_scores_panel(league_id)

        # when
        adapted = self.adapter.get_gameresults_response(panel)

        # then
        expect(adapted).to_length(num_games * results_per_game)
        for r in adapted:
            expect(len(r['scores'])).to_be_greater_than(0)