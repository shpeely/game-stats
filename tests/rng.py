import random
import sys

seed = random.randrange(sys.maxsize)
rng = random.Random(seed)

print('*********************')
print('RNG SEED:', seed)
print('*********************')
