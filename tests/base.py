import os
from unittest import TestCase
from tests.rng import seed
from game_stats.config import config
from glob import glob


class BaseTestCase(TestCase):

    def setUp(self):
        print()
        print('RNG seed: ', seed)
        print()

    def tearDown(self):
        # delete all hd5 files
        filelist = glob(os.path.join(config['datadir'], '*.hd5'))
        for f in filelist:
            os.remove(f)
