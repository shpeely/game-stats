from preggy import expect

from game_stats.controller import Controller
from game_stats.statistics import GameResultStats
from tests.base import BaseTestCase
from tests import testutils
from tests.rng import rng


class GameResultStatsTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.controller = Controller()
        self.game_result_stats = GameResultStats()

    def test_get_game_result_stats(self):
        # given
        panel, weights, result = self._create_game_results(
            players_per_game=4
        )

        # when
        stats = self.game_result_stats.get_game_result_stats(panel, weights, result.result_id)

        # then (sanity checks only)
        expect(stats['scores']).to_length(4)
        expect(stats['highscore']['score']).to_be_greater_than(stats['lowscore']['score'])
        expect(stats['average']).to_be_greater_than(stats['lowscore']['score'])
        expect(stats['average']).to_be_lesser_than(stats['highscore']['score'])

    def test_get_game_result_stats_single_result(self):
        # given
        league_id = 'test_league'
        result = testutils.create_fake_game_result(
            self.controller,
            'test',
            league_id=league_id,
            scores=[
                ('player-0', 10),
                ('player-1', 10),
                ('player-2', 20),
                ('player-3', 30),
                ('player-4', 30),
            ]
        )
        panel = self.controller.get_scores_panel(league_id)
        weights = self.controller.get_game_weights()

        # when
        stats = self.game_result_stats.get_game_result_stats(panel, weights, result.result_id)

        # then
        expect(stats['id']).to_equal(result.result_id)
        expect(stats['scores']).to_length(5)
        expect(stats['highscore']['score']).to_equal(30)
        expect(stats['lowscore']['score']).to_equal(10)
        expect(stats['lowscore']['players']).to_be_like(['player-0', 'player-1'])
        expect(stats['highscore']['players']).to_be_like(['player-3', 'player-4'])
        expect(stats['average']).to_equal(20)
        expect(stats['average_this_game']).to_equal(20)

        # winner's points should be equal loser's points multiplied with -1
        winner_points = next((x['points'] for x in stats['scores'] if x['score'] == 30))
        loser_points = next((x['points'] for x in stats['scores'] if x['score'] == 10))
        expect(winner_points).to_equal(-loser_points)

    def _create_game_results(
            self,
            num_games=5,
            max_score=100,
            players_per_game=4,
            league_id='test_league'
    ):
        fake_result_factory = testutils.FakeResultFactory(
            self.controller,
            num_players=5,
            num_games=1,
            max_score=max_score,
            players_per_game=players_per_game,
            league_id=league_id
        )
        results = fake_result_factory.create(num_games)

        panel = self.controller.get_scores_panel(league_id)

        weights = self.controller.get_game_weights()

        return panel, weights, results[rng.randint(0, num_games - 1)]

