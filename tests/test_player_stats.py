from preggy import expect

from game_stats.controller import Controller
from game_stats.statistics import PlayerStats
from tests.base import BaseTestCase
from tests import testutils


class PlayerStatsTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.controller = Controller()
        self.game_stats = PlayerStats()

    def tearDown(self):
        super().tearDown()

    def test_get_player_stats_no_results(self):
        # given
        league_id = 'test_league'

        # when
        stats = self.controller.get_player_stats(league_id)

        # then
        expect(stats.empty).to_be_true()

    def test_get_player_stats_number_of_results(self):
        # given
        league_id = 'test_league'
        num_results = 3
        num_players = 4
        fake_result_factory = testutils.FakeResultFactory(
            self.controller,
            num_players=num_players,
            players_per_game=num_players,
            league_id='test_league'
        )
        fake_result_factory.create(num_results)

        # when
        stats = self.controller.get_player_stats(league_id)

        # then
        expect(list(stats['number_of_results'])).to_equal([num_results] * num_players)

    def test_get_player_stats_sanity_checks(self):
        # given
        league_id = 'test_league'
        num_results = 10
        num_players = 10
        fake_result_factory = testutils.FakeResultFactory(
            self.controller,
            num_players=num_players,
            players_per_game=4,
            league_id='test_league'
        )
        fake_result_factory.create(num_results)

        # when
        stats = self.controller.get_player_stats(league_id)

        # then
        self._expect_all_players(stats['lose_ratio'] <= 1)
        self._expect_all_players(stats['win_ratio'] <= 1)
        self._expect_all_players(stats['number_of_results'] <= num_results)
        self._expect_all_players(stats['wins'] <= stats['number_of_results'])
        self._expect_all_players(stats['losses'] <= stats['number_of_results'])
        self._expect_all_players(stats['lowscores'] <= stats['number_of_results'])
        self._expect_all_players(stats['highscores'] <= stats['number_of_results'])

    def test_get_player_stats_high_and_lowscores(self):
        # given
        league_id = 'test_league'
        num_results = 3
        fake_result_factory = testutils.FakeResultFactory(
            self.controller,
            max_score=100,
            league_id='test_league'
        )
        results = fake_result_factory.create(num_results)

        # create a new game result with a highscore of 1000
        lucky_player = results[0].scores[0][0]
        unlucky_player = results[0].scores[1][0]

        new_scores = [
            (lucky_player, 1000),
            (unlucky_player, -1000),
            (results[0].scores[2][0], 50),
            (results[0].scores[3][0], 50),
        ]

        testutils.create_fake_game_result(
            self.controller,
            'test',
            league_id=league_id,
            game_id='new-result',
            scores=new_scores
        )

        # when
        stats = self.controller.get_player_stats(league_id)

        # then
        expect(stats['highscores'][lucky_player]).to_be_greater_or_equal_to(1)
        expect(stats['lowscores'][unlucky_player]).to_be_greater_or_equal_to(1)

    def test_get_player_stats_multiple_highscores(self):
        # given
        league_id = 'test_league'
        new_scores = [
            ('lucky-player-1', 1000),
            ('lucky-player-2', 1000),
            ('unlucky-player-1', -1000),
            ('unlucky-player-2', -1000),
        ]

        new_scores_2 = [
            ('lucky-player-1', 0),
            ('lucky-player-2', 0),
            ('unlucky-player-1', 0),
            ('unlucky-player-2', 0),
        ]

        testutils.create_fake_game_result(
            self.controller,
            'test',
            league_id=league_id,
            result_id='result-game-1',
            game_id='game-1',
            scores=new_scores
        )
        testutils.create_fake_game_result(
            self.controller,
            'test',
            league_id=league_id,
            result_id='result-game-2',
            game_id='game-2',
            scores=new_scores
        )

        testutils.create_fake_game_result(
            self.controller,
            'test',
            league_id=league_id,
            result_id='result-game-3',
            game_id='game-1',
            scores=new_scores_2
        )
        testutils.create_fake_game_result(
            self.controller,
            'test',
            league_id=league_id,
            result_id='result-game-4',
            game_id='game-2',
            scores=new_scores_2
        )

        # when
        stats = self.controller.get_player_stats(league_id)

        # then
        expect(stats['highscores']['lucky-player-1']).to_equal(2)
        expect(stats['highscores']['lucky-player-2']).to_equal(2)
        expect(stats['lowscores']['unlucky-player-1']).to_equal(2)
        expect(stats['lowscores']['unlucky-player-2']).to_equal(2)


    def _expect_all_players(self, condition):
        expect(condition.sum()).to_equal(condition.size)
