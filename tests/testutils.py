from datetime import datetime, timedelta
from game_stats.models import GameResult
from preggy import expect
from tests.rng import rng


def create_fake_game_result(controller, key, **kwargs):
    game_result = GameResult()

    game_result.league_id = kwargs.get('league_id', 'league-{}'.format(key))
    game_result.game_id = kwargs.get('game_id', 'game-{}'.format(key))
    game_result.game_weight = kwargs.get('game_weight', 1)
    game_result.result_id = kwargs.get('result_id', 'result-{}'.format(key))
    game_result.time = kwargs.get('time', datetime.now())

    if 'scores' in kwargs:
        game_result.scores = kwargs['scores']
    else:
        for i in range(0, kwargs.get('num_players', 4)):
            player_id = 'player-{}-{}'.format(key, i)
            game_result.add_score(player_id, kwargs.get('score', rng.randint(0, 100)))

    save_fake_game_result(controller, game_result)

    return game_result


def create_fake_game_results(controller, num_games, num_players, results_per_game, league_id='test_league'):
    """
    creates random game results. the players are randomly sampled from a list of generated
    player ids. Each game has min(num_players, 4) players/scores.

    :param controller:
    :param num_games: (int) the total number of games that are generated
    :param num_players: (int) total number of players
    :param results_per_game: (int) how many results per game
    :return:
    """
    results = []
    players = ['player-{}'.format(x) for x in range(0, num_players)]

    base_time = datetime.now()
    index = 0

    for game_index in range(0, num_games):
        for result_index in range(0, results_per_game):

            game_id = 'game-{}'.format(game_index)
            result_id = 'result-{}-{}'.format(result_index, game_index)

            # save the game result
            result = create_fake_game_result(
                controller,
                'test',
                league_id=league_id,
                game_id=game_id,
                result_id=result_id,
                scores=[(p, rng.randint(0, 100)) for p in rng.sample(players, min(len(players), 4))],
                time=base_time + timedelta(days=index),
            )
            results.append(result)
            index += 1

    return results


def save_fake_game_result(controller, game_result):
    # set the game weight to 1
    controller.set_game_weight(game_result.game_id, 1)

    controller.save_game_result(
        game_result.time,
        game_result.league_id,
        game_result.game_id,
        game_result.result_id,
        game_result.scores,
    )


def expect_attrs_to_equal(df, attr, expected):
    expect(df[attr].unique()).to_length(1)
    expect(df[attr].unique()).to_equal(expected)


class FakeResultFactory(object):

    def __init__(
            self,
            controller,
            num_players=5,
            num_games=5,
            max_score=100,
            players_per_game=4,
            league_id='test_league',
            result_index=0
    ):
        self.controller = controller
        self.num_players = num_players
        self.num_games = num_games
        self.max_score = max_score
        self.players_per_game = players_per_game
        self.result_index = result_index
        self.league_id = league_id

    def create(self, n, update_hdf=True):
        results = []
        players = ['player-{}'.format(x) for x in range(0, self.num_players)]

        base_time = datetime.now()

        for i in range(self.result_index, self.result_index + n):

            game_id = 'game-{}'.format(rng.randint(1, self.num_games))
            result_id = 'result-{}'.format(i)

            # save the game result
            result = create_fake_game_result(
                self.controller,
                'test',
                league_id=self.league_id,
                game_id=game_id,
                result_id=result_id,
                scores=[
                    (p, rng.randint(0, self.max_score))
                    for p in rng.sample(players, min(len(players), self.players_per_game))
                ],
                time=base_time + timedelta(days=i),
                update_hdf=update_hdf
            )
            results.append(result)

        self.result_index += n

        return results
