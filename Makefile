# This file is part of game-stats.
# https://github.com/omnibrain/game-stats

# Licensed under the MIT license:
# http://www.opensource.org/licenses/MIT-license
# Copyright (c) 2016, Raphael Voellmy <r.voellmy@gmail.com>

# lists all available targets
list:
	@sh -c "$(MAKE) -p no_targets__ | awk -F':' '/^[a-zA-Z0-9][^\$$#\/\\t=]*:([^=]|$$)/ {split(\$$1,A,/ /);for(i in A)print A[i]}' | grep -v '__\$$' | grep -v 'make\[1\]' | grep -v 'Makefile' | sort"
# required for list
no_targets__:

# install all dependencies (do not forget to create a virtualenv first)
setup:
	@pip install -e .\[tests\]

# test your application (tests in the tests/ directory)
test: unit

integration_tests: integration

unit:
	@python -m coverage run --branch `which nosetests` --with-yanc -s tests/
	@python -m coverage report -m

integration:
	@python `which nosetests` --with-yanc -s integration_tests/


# show coverage in html format
coverage-html: unit
	@python -m coverage html

# run tests against all supported python versions
tox:
	@tox

#docs:
	#@cd game_stats/docs && make html && open _build/html/index.html
