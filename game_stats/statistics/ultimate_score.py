import pandas as pd
import time
from game_stats.statistics.utils import *
from game_stats.constants import *

class UltimateScore:

    SCORE_MULTIPLIER = 300

    def __init__(self):
        pass

    def compute_ultimate_score_time_series_single_game(self, scores, weight):
        num_players = len(scores.index)
        num_games = len(scores.columns)

        # create 1D array of the scores and compute the moving mean and std.
        score_array = scores.values.transpose().ravel()
        rolling = pd.Series(score_array).rolling(window=(num_games * num_players), min_periods=1)

        # get every (num_players)th value of the rolling array, which corresponds to the new
        # value after the game has been played
        means = rolling.mean().iloc[(num_players - 1)::num_players].values
        stds = rolling.std().iloc[(num_players - 1)::num_players].values

        # create the multiplication matrix for the results
        multiplication_matrix = scores.notnull().cumsum(axis=1)

        # Apply the formula:
        # \frac{(\sum_{i=1}^N x_i) - i*\mu_i}{std_i}
        score_cummulated = scores.cumsum(axis=1, skipna=True).ffill(axis=1)

        result = (score_cummulated - (means * multiplication_matrix)) / stds

        # compute the differences and save it in the last column
        diff = result.fillna(value=0).diff(axis=1)
        diff.iloc[:, :1] = result.iloc[:, 0:1]
        diff = diff.fillna(value=0)

        return diff * (self.SCORE_MULTIPLIER * weight)

    def update_ultimate_score_timeseries(self, panel, timeseries, metadata, weights, time, game_id, result_id, player_scores):

        game_weight = weights[game_id]
        metadata = self.set_created_date(metadata, result_id, time)

        game_player_id = get_game_player_id(game_id, len(player_scores))

        # if gameresult exists already, remove it from the panel and timeseries first
        if result_id in panel.index.get_level_values(RESULT_LEVEL):
            panel = self.delete_gameresult(panel, result_id)
            del timeseries[result_id]

        panel = panel.append(pd.Series(
            {(game_player_id, result_id, s[0]): s[1] for s in player_scores}
        ))

        # reindex the minor_axis by created time
        sorted_index = metadata.sort_values(['created']).index

        panel_index = panel.index.get_level_values(RESULT_LEVEL).drop_duplicates()

        if not sorted_index.equals(panel_index):
            # game result was not added in order -> reindex panel and recompute timeseries
            index_df = panel.index.to_frame()
            index_df[RESULT_LEVEL] = pd.Index(index_df[RESULT_LEVEL], sorted_index)
            index_df = index_df.reset_index(drop=True).sort_values(RESULT_LEVEL)
            new_index = pd.MultiIndex.from_frame(index_df)
            panel = panel.reindex(index=new_index)

            timeseries = self.recompute_ultimate_score_time_series(panel, weights)
            return panel, timeseries, metadata

        else:
            # gameresult was added in order -> only need to compute score for the last row
            game_df = panel[game_player_id].unstack(level=0)
            diff = self.compute_ultimate_score_time_series_single_game(game_df, game_weight)

            if timeseries.empty:
                timeseries = diff
            else:
                timeseries = pd.concat([timeseries, diff.iloc[:, -1]], axis=1, sort=True)
                timeseries.iloc[:, -2:] = timeseries.iloc[:, -2:].fillna(value=0).cumsum(axis=1)

            return panel, timeseries.fillna(value=0), metadata

    def compute_single_game_result(self, panel, weights, result_id):
        """
        Comupte the ultimate score of a specific game result
        :param panel:
        :param weights:
        :param result_id:
        :return: Serie of the ultimate score gained/lost by a specific game result
        """
        result = panel.loc[:, result_id, :].unstack(level=0)
        game_player_id = result.columns[0]
        values = panel.loc[game_player_id, :, :].values

        mean, std = values.mean(), values.std()
        game_weight = weights[get_game_id_from_game_player_id(game_player_id)]

        points = game_weight * (result - mean) * self.SCORE_MULTIPLIER / std

        # return series
        return points.squeeze()

    def recompute_ultimate_score_time_series(self, panel, weights):
        start = time.time()

        game_player_ids = panel.index.get_level_values(GAME_LEVEL).unique()

        diffs = []
        for id in game_player_ids:
            df = panel.loc[id, :, :]

            if GAME_LEVEL in df.index.names:
                # this is weird inconsistent behavior of pandas. For some reason the game
                # level is sometimes still there and sometimes it's not. To get a consistent
                # result I'm removing the level here.
                df = df.droplevel(GAME_LEVEL)

            scores = df.unstack().transpose()
            diff = self.compute_ultimate_score_time_series_single_game(
                scores,
                weights[get_game_id_from_game_player_id(id)]
            )
            diffs.append(diff)

        # concat the tables, fill empty values, group by player and sum all up.
        final = pd.concat(diffs, copy=False, sort=False)

        # re-order final diffs according to the actual order of the results
        final = final[panel.index.get_level_values(RESULT_LEVEL).unique()]

        final = final.fillna(value=0).groupby(final.index).sum().cumsum(axis=1)

        print('Time for computation (in seconds):', time.time() - start)

        return final

    def delete_gameresult(self, panel, result_id):
        return panel.drop(result_id, level=RESULT_LEVEL)

    def set_created_date(self, metadata, result_id, time):
        metadata.loc[result_id, 'created'] = time.replace(tzinfo=None)
        return metadata