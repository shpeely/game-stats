import pandas as pd

from game_stats.constants import PLAYER_LEVEL, GAME_LEVEL


class PlayerStats:
    def __init__(self):
        pass

    def player_stats(self, panel):
        if panel.empty:
            return pd.DataFrame()

        num_results = panel.index.to_frame()[PLAYER_LEVEL].value_counts()

        wins_per_player = panel.unstack().idxmax(axis=1).value_counts()
        losses_per_player = panel.unstack().idxmin(axis=1).value_counts()
        highscores = self._get_minmax(panel, True)
        lowscores = self._get_minmax(panel, False)

        # create dataframe from the computed series
        result_df = pd.concat([
            num_results,
            wins_per_player,
            losses_per_player,
            highscores,
            lowscores,

        ], axis=1, sort=True)
        result_df.columns = [
            'number_of_results',
            'wins',
            'losses',
            'highscores',
            'lowscores',
        ]
        result_df.fillna(value=0, inplace=True)

        result_df['win_ratio'] = result_df['wins'] / result_df['number_of_results']
        result_df['lose_ratio'] = result_df['losses'] / result_df['number_of_results']

        return result_df

    def _get_minmax(self, panel, max):
        # for each game calculate each players high- and lowscore
        minmax = panel.unstack(level=0).max() if max else panel.unstack(level=0).min()

        return (panel.unstack(level=GAME_LEVEL) == minmax)\
            .sum(axis='columns')\
            .groupby(level=PLAYER_LEVEL)\
            .sum()