import pandas as pd
from game_stats.statistics.utils import *
from game_stats.statistics.game_stats import GameStats
from game_stats.statistics.ultimate_score import UltimateScore

class GameResultStats:

    def __init__(self):
        self.game_stats = GameStats()
        self.ultimate_score = UltimateScore()


    def get_game_result_stats(self, panel, weights, result_id):
        result_df = panel.loc[:, result_id, :].unstack(level=0)
        game_player_id = result_df.columns[0]

        game_id = get_game_id_from_game_player_id(game_player_id)
        num_players = get_num_players_from_game_player_id(game_player_id)

        scores_df = result_df[game_player_id].rename('score')
        points = self.ultimate_score.compute_single_game_result(panel, weights, result_id).rename('points')

        scores_with_points_df = pd.concat([scores_df, points], axis=1)
        scores_with_points = [{
            'player': player,
            'score': float(scores_with_points_df.loc[player, 'score']),
            'points': float(scores_with_points_df.loc[player, 'points']),
        } for player in scores_with_points_df.index]

        highscore, highscore_players = self.game_stats.get_game_highscores(
            panel,
            game_id,
            num_players=num_players
        )
        lowscore, lowscore_players = self.game_stats.get_game_lowscores(
            panel,
            game_id,
            num_players=num_players
        )

        average = self.game_stats.get_game_means(panel, game_id, num_players=num_players)
        average_this_game = self.get_average_score(panel, result_id)

        return {
            'id': result_id,
            'scores': scores_with_points,
            'average': average,
            'average_this_game': average_this_game,
            'highscore': {
                'score': float(highscore),
                'players': highscore_players
            },
            'lowscore': {
                'score': float(lowscore),
                'players': lowscore_players
            }
        }

    def get_average_score(self, panel, result_id):
        return panel.loc[:, result_id, :].mean()