import pandas as pd
from game_stats.statistics.utils import *
from game_stats.constants import *

class GameStats:
    def __init__(self):
        pass

    def game_stats(self, panel):
        if panel.size == 0:
            return []
        games_count = self.games_count(panel)
        highscores = self.highscores(panel)
        lowscores = self.lowscores(panel)

        return [ {
            'game_id': game,
            'count': int(games_count.loc[game]),
            'highscores': highscores[game],
            'lowscores': lowscores[game],
        } for game in games_count.index]

    def games_count(self, panel):
        # get number of results per game
        num_scores = panel.droplevel('players').index.to_frame().reset_index(drop=True).groupby('games').agg('count')

        game_player_ids = panel.index.get_level_values(GAME_LEVEL).unique()
        num_players = pd.Series(
            data=[get_num_players_from_game_player_id(x) for x in game_player_ids],
            index=game_player_ids
        )

        num_games = (num_scores.transpose() // num_players).transpose()
        num_games.index = num_games.index.map(get_game_id_from_game_player_id)

        return num_games.groupby(level=0).sum()

    def highscores(self, panel):
        return self._get_minmax(panel, True)

    def lowscores(self, panel):
        return self._get_minmax(panel, False)

    def _get_minmax(self, panel, max):
        scores = panel.unstack(level=0)
        minmax = scores.max() if max else scores.min()

        minmax_per_game_players = {
            game_player_id: list(panel.loc[game_player_id][lambda x: x == minmax[game_player_id]].index.get_level_values(PLAYER_LEVEL))
            for game_player_id in minmax.index
        }

        result = {}
        for game_id, minmaxes in minmax_per_game_players.items():
            game = get_game_id_from_game_player_id(game_id)
            result[game] = result.get(game, []) + minmaxes

        return result

    def get_game_means(self, panel, game_id, num_players=None):
        """
        Returns the mean scores for a specific game
        :param panel: the scores panel
        :param game_id: the game id
        :param num_players: number of players
        :return: A tuple of (<num players>, <mean>) or just the mean if num_players is provided
        """
        game_player_ids = self._get_game_player_ids(panel, game_id)
        means = [
            (get_num_players_from_game_player_id(id), self._compute_mean_single_game(panel, id))
            for id in game_player_ids
        ]
        if num_players:
            return next((x for x in means if x[0] == num_players))[1]
        else:
            return means

    def compute_mean_single_game(self, panel, game_id, num_players):
        return self._compute_mean_single_game(panel, get_game_player_id(game_id, num_players))

    def get_game_highscores(self, panel, game_id, num_players=None):
        return self._get_game_minmax(panel, game_id, True, num_players)

    def get_game_lowscores(self, panel, game_id, num_players=None):
        return self._get_game_minmax(panel, game_id, False, num_players)

    def _get_game_minmax(self, panel, game_id, max, num_players):
        """
        :param panel: The scores panel
        :param game_id: the game id
        :param num_players: number of players
        :return: A tuple of (<num players>, (<score>, [<player_ids>])) or
            just (<score>, [<player_ids>]) if num_players is provided
        """
        game_player_ids = self._get_game_player_ids(panel, game_id)
        maxima_scores = [
            (get_num_players_from_game_player_id(id), self._compute_minmax_single_game(panel, id, max))
            for id in game_player_ids
        ]
        if num_players:
            return next((x for x in maxima_scores if x[0] == num_players))[1]
        else:
            return maxima_scores

    def _compute_mean_single_game(self, panel, game_player_id):
        return panel.loc[game_player_id, :, :].mean()

    def _compute_minmax_single_game(self, panel, game_player_id, max):
        scores = panel.loc[game_player_id, :, :].unstack(level=0)
        highscore_value = scores.stack().max() if max else scores.stack().min()
        highscores = scores \
            .stack() \
            .where(scores.stack() == highscore_value) \
            .dropna()
        players = list(set(highscores.index.get_level_values(PLAYER_LEVEL)))
        return (highscore_value, players)

    def _get_game_player_ids(self, panel, game_id):
        game_player_ids = panel.index.get_level_values(GAME_LEVEL).unique()
        return list(filter(lambda x: get_game_id_from_game_player_id(x) == game_id, game_player_ids))
