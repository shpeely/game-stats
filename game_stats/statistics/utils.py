GAME_PLAYER_ID_SEPARATOR = '__'

def get_game_player_id(game_id, num_players):
    return '{}{}{}'.format(game_id, GAME_PLAYER_ID_SEPARATOR, num_players)

def get_game_id_from_game_player_id(game_player_id):
    return game_player_id.split(GAME_PLAYER_ID_SEPARATOR)[0]

def get_num_players_from_game_player_id(game_player_id):
    return int(game_player_id.split(GAME_PLAYER_ID_SEPARATOR)[1])