import os
import logging

config = {
    'env': os.environ.get('ENV', 'development'),
    'rabbitmq': {
        'host': os.environ.get('RABBITMQ_HOST', 'localhost'),
        'port': os.environ.get('RABBITMQ_PORT', 5672),
        'rpc_queue': os.environ.get('RABBITMQ_QUEUE', 'game_stats_rpc_queue'),
    },
    'flask': {
        'port': os.environ.get('FLASK_PORT', 5000)
    },
    'logging': {
        'level': logging.DEBUG,
        'format': '%(levelname) -10s %(asctime)s %(name) -30s %(funcName) -35s %(lineno) -5d: %(message)s'
    },
    'datadir': os.environ.get('DATA_DIR', '.'),
    'store': {
        'provider': os.environ.get('STORAGE_PROVIDER', 'fs'),
        's3_bucket': os.environ.get('S3_BUCKET', 'shpeely'),
        'aws_key_id': os.environ.get('AWS_ACCESS_KEY_ID'),
        'aws_secret_access_key': os.environ.get('AWS_SECRET_ACCESS_KEY'),
    }
}