from game_stats.store.fs_store import FsStore
from game_stats.store.s3_store import S3Store


class StoreFactory(object):

    @staticmethod
    def get_store(provider):
        if provider == 'fs':
            return FsStore()
        elif provider == 's3':
            return S3Store()
        else:
            raise Exception('Unknown storage provider ' + provider)