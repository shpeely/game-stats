import os

import boto3
import logging
import pandas as pd
import tempfile
from botocore.exceptions import ClientError

from game_stats.config import config
from game_stats.store.store import Store

LOGGER = logging.getLogger(__name__)

class S3Store(Store):

    def __init__(self):
        self.s3client = boto3.client('s3')
        self.bucket = config['store']['s3_bucket']

    def save(self, filename, data):
        tmp = tempfile.mktemp(suffix='.md5')
        data.to_hdf(tmp, key=self._key(filename))

        try:
            self.s3client.upload_file(tmp, self.bucket, filename)
        except ClientError as e:
            LOGGER.error(e)

        os.remove(tmp)

    def load(self, filename):
        tmp = tempfile.mktemp(suffix='.md5')
        self.s3client.download_file(self.bucket, filename, tmp)

        return pd.read_hdf(tmp)

    def exists(self, filename):
        """return the key's size if it exist, else None"""
        response = self.s3client.list_objects_v2(
            Bucket=self.bucket,
            Prefix=filename,
        )
        for obj in response.get('Contents', []):
            if obj['Key'] == filename:
                return True

    def list(self):
        return [obj['Key'] for obj in self.s3client.list_objects(Bucket=self.bucket)['Contents']]