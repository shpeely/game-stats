from game_stats.config import config
import os
import pandas as pd

from game_stats.store.store import Store


class FsStore(Store):

    def save(self, filename, data):
        data.to_hdf(self._abs_path(filename), key=self._key(filename))

    def load(self, filename):
        return pd.read_hdf(self._abs_path(filename))

    def exists(self, filename):
        return os.path.isfile(filename)

    def list(self):
        return os.listdir(config['datadir'])

    def _abs_path(self, filename):
        return os.path.join(config['datadir'], filename)
