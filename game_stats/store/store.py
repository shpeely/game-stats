import re

class Store(object):

    def save(self, df, filename):
        raise NotImplementedError()

    def load(self, filename):
        raise NotImplementedError()

    def _key(self, filename):
        return re.sub('[^A-Za-z0-9]+', '', filename)