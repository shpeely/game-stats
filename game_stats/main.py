import logging

from game_stats.config import config
from game_stats.controller import Controller
from game_stats.connection import HttpConnection

def start():
    # configure logging
    logging.basicConfig(level=config['logging']['level'], format=config['logging']['format'])

    # start application
    controller = Controller()
    connection = HttpConnection(controller)
    connection.run()