import logging
from flask import request
from flask_api import FlaskAPI, status
import ciso8601
from game_stats.adapters import PythonAdapter
from game_stats.config import config
from game_stats.connection.event import Query, Mutation

from game_stats.connection import Connection

LOGGER = logging.getLogger(__name__)
app = FlaskAPI(__name__)

connection = None

class HttpConnection(Connection):

    def __init__(self, controller):
        global connection
        super().__init__(controller, PythonAdapter())
        self.controller = controller
        connection = self

    def run(self):
        app.run(
            debug=(config['env'] != 'production'),
            host='0.0.0.0',
            port=config['flask']['port']
        )

    def app(self):
        global app
        return app


# helpers
def _respond_success(data=None):
    if data:
        return data
    else:
        return { 'result': 'success' }


# endpoints


@app.route('/', methods=['GET'])
def root_route():
    return ''

@app.route('/league', methods=['GET'])
def leagues():
    return connection.handle_request(Query.get_leagues)

@app.route('/league/<string:league_id>/timeseries', methods=['GET'])
def timeseries(league_id):
    return connection.get_timeseries({ 'league' : league_id })

@app.route('/league/<string:league_id>/gameresult/<string:result_id>',  methods=['DELETE', 'PUT'])
def gameresult(league_id, result_id):
    if request.method == 'PUT':
        game_result = { **request.data }
        game_result['league_id'] = league_id
        game_result['time'] = ciso8601.parse_datetime(game_result['time'])
        game_result['id'] = result_id
        connection.handle_request(
            Mutation.save_game_result,
            game_result
        )
        return  _respond_success()

    if request.method == 'DELETE':
        connection.handle_request(
            Mutation.delete_gameresult,
            { 'league_id': league_id, 'result_id': result_id }
        )
        return _respond_success()


@app.route('/league/<string:league_id>/gameresult',  methods=['POST', 'GET'])
def gameresults(league_id):
    """
    Add game result.

    Expects a json object like this:
    ```
    {
        "bggid": "<bggid>",
        "id": "<gameresultId>",
        "time": "<iso8601 date string>"
        "scores": [
            { "player" : "<playerId>, "score" : 100 },
            { "player" : "<playerId>, "score" : 200 },
            { "player" : "<playerId>, "score" : 300 }
        ]
    }
    ```
    """
    if request.method == 'POST':
        game_result = { **request.data }
        game_result['league_id'] = league_id
        game_result['time'] = ciso8601.parse_datetime(game_result['time'])
        connection.handle_request(
            Mutation.save_game_result,
            game_result
        )
        return  _respond_success(), status.HTTP_201_CREATED

    return connection.handle_request(
        Query.get_gameresults, league_id
    )


@app.route('/game/<string:game_id>/weight/<string:weight>',  methods=['POST'])
def set_weight(game_id, weight):
    """
    Set the weight of a game
    """
    connection.handle_request(
        Mutation.set_game_weight,
        { 'game_id' : game_id, 'weight' : float(weight) }
    )
    return  _respond_success()

@app.route('/game/<string:game_id>/weight',  methods=['GET'])
def get_weight(game_id):
    """
    Get the weight of a game
    """
    connection.handle_request(
        Query.get_game_weight, game_id
    )
    return  _respond_success()

@app.route('/league/<string:league_id>/meta',  methods=['GET'])
def get_meta(league_id):
    """
    Get meta data of a league
    """
    meta = connection.handle_request(
        Query.get_meta, league_id
    )
    return meta

@app.route('/league/<string:league_id>/gameresult/<string:result_id>/stats',  methods=['GET'])
def get_game_result_stats(league_id, result_id):
    """
    Get statistics for a single game result
    """
    meta = connection.handle_request(
        Query.get_game_result_stats, league_id, result_id
    )
    return meta


@app.route('/league/<string:league_id>/games/stats',  methods=['GET'])
def get_game_stats(league_id):
    """
    Get statistics for all games of a league
    """
    stats = connection.handle_request(
        Query.get_game_stats, league_id
    )
    return stats

@app.route('/league/<string:league_id>/players/stats',  methods=['GET'])
def get_player_stats(league_id):
    """
    Get statistics for all players of a league
    """
    stats = connection.handle_request(
        Query.get_player_stats, league_id
    )
    return stats

@app.route('/meta', methods=['GET', 'POST'])
def global_meta():
    """
    Get global meta data
    """
    if request.method == 'GET':
        key = request.args.get('key')
        return connection.handle_request(
            Query.get_global_meta,
            key
        )

    # convert iso dates to datetime
    def to_datetime_if_iso8601(input):
        try:
            datetime = ciso8601.parse_datetime(input)
            return datetime if datetime else input
        except:
            return input

    meta = { k:to_datetime_if_iso8601(v) for k, v in request.data.items() }

    return connection.handle_request(
        Mutation.set_global_meta,
        meta
    )


@app.route('/ping/',  methods=['GET'])
def ping():
    return _respond_success()