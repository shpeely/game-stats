from enum import Enum

class Response(Enum):
    ok = 'OK'
    error = 'ERROR'
