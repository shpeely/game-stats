import logging
from game_stats.connection import Connection
import json

LOGGER = logging.getLogger(__name__)

class JsonConnection(Connection):
    """
    Same as the Connection class but takes data as JSON
    """

    def handle_request(self, command, data):
        return self.handlers[command](json.loads(data))