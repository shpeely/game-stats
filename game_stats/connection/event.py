from enum import Enum

class Mutation(Enum):
    save_game_result = 'save_game_result'
    set_game_weight = 'set_game_weight'
    set_global_meta = 'set_global_meta'
    delete_gameresult = 'delete_gameresult'

class Query(Enum):
    get_meta = 'get_meta'
    get_global_meta = 'get_global_meta'
    get_timeseries = 'get_timeseries'
    get_game_weight = 'get_game_weight'
    get_leagues = 'get_leagues'
    get_gameresults = 'get_gameresults'
    get_game_result_stats = 'get_game_result_stats'
    get_game_stats = 'get_game_stats'
    get_player_stats = 'get_player_stats'
