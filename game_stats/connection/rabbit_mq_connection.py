import pika
import logging
import threading
from game_stats.config import config
from game_stats.connection import Connection

LOGGER = logging.getLogger(__name__)

class RabbitMQ(Connection):
    """
    RabbitMQ connection base setup. Not used but I'm keeping it.
    """

    HOST = config['rabbitmq']['host']
    QUEUE = config['rabbitmq']['rpc_queue']

    def __init__(self, controller, adapter):
        self.controller = controller
        self.adapter = adapter

    def run(self):
        self._stop_event = threading.Event()
        self._connection = pika.BlockingConnection(pika.ConnectionParameters(host=self.HOST))
        self._channel = self._connection.channel()
        self._channel.queue_declare(queue=self.QUEUE)
        self._channel.basic_qos(prefetch_count=1)
        self._channel.basic_consume(self.on_request, queue=self.QUEUE)
        self.thread = threading.Thread(target = self.start, daemon=True)
        self.thread.start()

    def start(self):
        try:
            self._channel.start_consuming()
        finally:
            self._channel.close()

    def on_request(self, ch, method, props, body):
        response = 'pong'

        ch.basic_publish(exchange='',
                         routing_key=props.reply_to,
                         properties=pika.BasicProperties(correlation_id = \
                                                             props.correlation_id),
                         body=str(response))
        ch.basic_ack(delivery_tag = method.delivery_tag)