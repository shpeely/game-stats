import logging
from datetime import datetime
from game_stats.connection.event import Mutation, Query
from game_stats.connection.response import Response

LOGGER = logging.getLogger(__name__)

class Connection(object):

    def __init__(self, controller, adapter):
        self.controller = controller
        self.adapter = adapter

        self._register_command_handlers()

    def handle_request(self, command, *args):
        return self.handlers[command](*args)

    def save_game_result(self, result):
        result = self.controller.save_game_result(
            result['time'],
            result['league_id'],
            result['bggid'],
            result['id'],
            list(map(lambda x: (x['player'], x['score']), result['scores']))
        )
        return self.adapter.save_game_result_response(result)

    def update_game_result(self, result):
        return self.save_game_result(result)

    def set_game_weight(self, data):
        self.controller.set_game_weight(data['game_id'], data['weight'])
        return self.adapter.set_game_weight_response(Response.ok)

    def get_game_weight(self, data):
        self.controller.get_game_weight(game_id=data.get('game_id'))

    def get_timeseries(self, data):
        timeseries = self.controller.get_timeseries(data['league'])
        return self.adapter.get_timeseries_response(timeseries)

    def get_meta(self, league_id):
        meta = self.controller.get_metadata(league_id)
        return self.adapter.get_meta_response(meta)

    def get_global_meta(self, key):
        meta = self.controller.get_global_meta(key)
        return self.adapter.get_global_meta_response(meta)

    def set_global_meta(self, meta):
        meta = self.controller.set_global_meta_multiple(meta)

        # return all meta data
        return self.adapter.set_global_meta_response(meta)

    def get_leagues(self):
        return self.controller.get_leagues()

    def delete_gameresult(self, data):
        return self.controller.delete_gameresult(data['league_id'], data['result_id'])

    def get_gameresults(self, league_id):
        return self.adapter.get_gameresults_response(self.controller.get_scores_panel(league_id))

    def get_game_result_stats(self, league_id, result_id):
        return self.adapter.get_game_result_stats_response(self.controller.get_game_result_stats(league_id, result_id))

    def get_game_stats(self, league_id):
        return self.adapter.get_game_stats_response(self.controller.get_game_stats(league_id))

    def get_player_stats(self, league_id):
        return self.adapter.get_player_stats_response(self.controller.get_player_stats(league_id))

    def _register_command_handlers(self):
        self.handlers = {
            Query.get_timeseries: self.get_timeseries,
            Query.get_game_weight: self.get_game_weight,
            Query.get_meta: self.get_meta,
            Query.get_global_meta: self.get_global_meta,
            Query.get_leagues: self.get_leagues,
            Query.get_gameresults: self.get_gameresults,
            Query.get_game_result_stats: self.get_game_result_stats,
            Query.get_game_stats: self.get_game_stats,
            Query.get_player_stats: self.get_player_stats,
            Mutation.save_game_result: self.save_game_result,
            Mutation.set_game_weight: self.set_game_weight,
            Mutation.set_global_meta: self.set_global_meta,
            Mutation.delete_gameresult: self.delete_gameresult
        }