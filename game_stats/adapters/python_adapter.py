from game_stats.adapters import Adapter
from game_stats.constants import GAME_LEVEL
from game_stats.statistics.ultimate_score import GAME_PLAYER_ID_SEPARATOR

class PythonAdapter(Adapter):
    """
    Converts pandas data to python objects (dicts or lists)
    """

    def get_timeseries_response(self, timeline):
        timeline = timeline.applymap(int)
        results = list(timeline.columns)
        players = list(timeline.index)
        scores = {}
        for player in players:
            # create array of integers from the player's scores
            scores[player] = list(map(int, timeline.loc[player].values))

        return {
            'gameresults': results,
            'scores': scores
        }

    def get_meta_response(self, meta):
        return meta.to_dict()

    def get_gameresults_response(self, panel):
        # iterate over results and build list of dicts from it
        results = []
        for result_id, scores_df in [(result_id, panel.loc[:, result_id, :].unstack(level=GAME_LEVEL)) for result_id in panel.index.get_level_values('results').unique()]:
            [game_id, _] = scores_df.columns[0].split(GAME_PLAYER_ID_SEPARATOR)
            results.append({
                'id': result_id,
                'game_id': game_id,
                'scores': [
                    { 'player': p, 'score': s }
                    for p, s in scores_df.iloc[:, 0].to_dict().items()
                ]
            })

        return results

    def get_game_result_stats_response(self, game_result_stats):
        return game_result_stats

    def get_game_stats_response(self, game_stats):
        return game_stats

    def get_player_stats_response(self, player_stats):
        player_stats['player'] = player_stats.index
        return player_stats.to_dict('records')
