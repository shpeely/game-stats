class Adapter(object):
    """
    Base adapter. Doesn't do any transformation and returns
    """

    def save_game_result_response(self, scores):
        return scores

    def get_timeseries_response(self, timeline):
        return timeline

    def set_game_weight_response(self, result):
        return result

    def get_meta_response(self, meta):
        return meta

    def get_global_meta_response(self, meta):
        return meta

    def set_global_meta_response(self, meta):
        return meta

    def get_gameresults_response(self, data):
        return data

    def get_game_result_stats_response(self, data):
        return data

    def get_game_stats_response(self, data):
        return data

    def get_player_stats_response(self, data):
        return data