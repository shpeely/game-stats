# Game Stats

Generically compute statistics for any games.

[![build status](https://gitlab.com/shpeely/game-stats/badges/master/build.svg)](https://gitlab.com/shpeely/game-stats/commits/master)

[![coverage report](https://gitlab.com/shpeely/game-stats/badges/master/coverage.svg)](https://shpeely.gitlab.io/game-stats)