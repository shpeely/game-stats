import pika
import uuid
import os
import time
from game_stats.config import config
from unittest import TestCase as PythonTestCase

class TestCase(PythonTestCase):
    pass

class RabbitMQTestCase(TestCase):

    connection = None

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        # set up redditmq
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host=config['rabbitmq']['host']))
        self.channel = self.connection.channel()
        result = self.channel.queue_declare(exclusive=True)
        self.callback_queue = result.method.queue
        self.channel.basic_consume(self._on_response, no_ack=True, queue=self.callback_queue)

    def tearDown(self):
        # close rabbitmq connection
        self.connection.close()

        # delete all hd5 files
        filelist = [ f for f in os.listdir('.') if f.endswith('.hd5') ]
        for f in filelist:
            os.remove(f)

    def _on_response(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            self.response = body

    def rpc_call(self, message):
        self.response = None
        self.corr_id = str(uuid.uuid4())
        self.channel.basic_publish(exchange='',
                                   routing_key=config['rabbitmq']['rpc_queue'],
                                   properties=pika.BasicProperties(
                                       reply_to = self.callback_queue,
                                       correlation_id = self.corr_id,
                                   ),
                                   body=message)
        while self.response is None:
            self.connection.process_data_events()
            time.sleep(0.01)

        return self.response