from game_stats.store import StoreFactory
from tests.base import BaseTestCase
import pandas as pd
from preggy import expect
from game_stats.config import config
import unittest

@unittest.skipUnless(
    config['store']['aws_key_id'] and config['store']['aws_secret_access_key'] and config['store']['s3_bucket'],
    'Skipping because AWS configuration is missing'
)
class S3StoreTestCase(BaseTestCase):

    def setUp(self):
        super().setUp()
        self.store = StoreFactory.get_store('s3')

    def test_save_and_load(self):
        # given
        filename = 'test.hd5'
        df = pd.DataFrame([1, 2, 3])

        # when
        self.store.save(filename, df)

        # then
        expect(self.store.load(filename).equals(df)).to_equal(True)

    def test_exists(self):
        # given
        filename = 'test.hd5'
        self.store.save(filename, pd.DataFrame())

        # then
        expect(self.store.exists(filename)).to_equal(True)

    def test_list(self):
        # given
        filenames = ['test' + str(i) + '.hd5' for i in range(3)]

        # when
        for name in filenames:
            self.store.save(name, pd.DataFrame())

        # then
        expect(set(filenames).issubset(self.store.list())).to_equal(True)
