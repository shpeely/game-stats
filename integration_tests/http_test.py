import os
from unittest import TestCase as PythonTestCase
from game_stats.connection import HttpConnection
from game_stats.controller import Controller

class TestCase(PythonTestCase):
    pass

class HttpBaseTest(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.controller = Controller()
        cls.connection = HttpConnection(cls.controller)
        cls.app = cls.connection.app().test_client()

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        pass

    def tearDown(self):
        # delete all hd5 files
        filelist = [ f for f in os.listdir('.') if f.endswith('.hd5') ]
        for f in filelist:
            os.remove(f)