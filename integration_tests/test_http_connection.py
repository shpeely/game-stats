from integration_tests.http_test import HttpBaseTest
import json
import pytz
from datetime import datetime
from unittest.mock import MagicMock
from game_stats.connection import Mutation, Query

class HttpTestCase(HttpBaseTest):

    def setUp(self):
        super().setUp()

    def tearDown(self):
        super().tearDown()

    def test_save_game_result(self):
        # given
        self.connection.handle_request = MagicMock()
        result = {
            'bggid': '123',
            'id': 'result-id',
            'time': '2000-01-01T00:00:00.000Z',
            'scores': [
                { 'player': 'player1', 'score': 10 },
                { 'player': 'player2', 'score': 20 },
            ]
        }

        # when
        self.app.post('/league/test-league/gameresult',
                      data=json.dumps(result),
                      content_type='application/json',
                      )

        # then
        expected = {
            **result,
            'league_id': 'test-league',
            'time': datetime(2000, 1, 1, 0, 0, 0, 0, pytz.UTC),
        }
        self.connection.handle_request.assert_called_with(
            Mutation.save_game_result,
            expected
        )
    def test_update_game_result(self):
        # given
        self.connection.handle_request = MagicMock()
        result = {
            'bggid': '123',
            'time': '2000-01-01T00:00:00.000Z',
            'scores': [
                { 'player': 'player1', 'score': 10 },
                { 'player': 'player2', 'score': 20 },
            ]
        }

        self.app.put('/league/test-league/gameresult/result-id',
                      data=json.dumps(result),
                      content_type='application/json',
                      )

        # then
        expected = {
            **result,
            'id': 'result-id',
            'league_id': 'test-league',
            'time': datetime(2000, 1, 1, 0, 0, 0, 0, pytz.UTC),
        }
        self.connection.handle_request.assert_called_with(
            Mutation.save_game_result,
            expected
        )

    def test_set_game_weight(self):
        self.connection.handle_request = MagicMock()
        self.app.post('/game/123/weight/3.1415')
        self.connection.handle_request.assert_called_with(
            Mutation.set_game_weight,
            { 'weight': 3.1415, 'game_id': '123' }
        )

    def test_get_game_weight(self):
        self.connection.handle_request = MagicMock()
        self.app.get('/game/123/weight')
        self.connection.handle_request.assert_called_with(
            Query.get_game_weight, '123'
        )

    def test_get_meta(self):
        self.connection.handle_request = MagicMock(return_value={})
        self.app.get('/league/test-league/meta')
        self.connection.handle_request.assert_called_with(
            Query.get_meta, 'test-league'
        )

    def test_set_global_meta(self):
        # given
        meta = { 'test': 123 }
        self.connection.handle_request = MagicMock(return_value=meta)

        # when
        self.app.post('/meta', data=json.dumps(meta), content_type='application/json')

        # then
        self.connection.handle_request.assert_called_with(
            Mutation.set_global_meta, meta
        )

    def test_set_global_meta_datetime(self):
        # given
        date = datetime.now(tz=pytz.timezone('UTC')).astimezone()
        meta = { 'date': date.isoformat() }
        self.connection.handle_request = MagicMock(return_value=meta)

        # when
        self.app.post('/meta', data=json.dumps(meta), content_type='application/json')

        # then
        self.connection.handle_request.assert_called_with(
            Mutation.set_global_meta, { 'date': date }
        )

    def test_get_global_meta(self):
        # given
        meta = { 'test': 123 }
        self.connection.handle_request = MagicMock(return_value=meta)

        # when
        self.app.get('/meta?key=test')

        # then
        self.connection.handle_request.assert_called_with(
            Query.get_global_meta, 'test'
        )

    def test_get_leagues(self):
        # given
        self.connection.handle_request = MagicMock(return_value=[])

        # when
        self.app.get('/league')

        # then
        self.connection.handle_request.assert_called_with(
            Query.get_leagues
        )

    def test_delete_gameresult(self):
        # given
        self.connection.handle_request = MagicMock()

        # when
        self.app.delete('/league/test-league/gameresult/test-result')

        # then
        self.connection.handle_request.assert_called_with(
            Mutation.delete_gameresult,
            { 'league_id': 'test-league', 'result_id': 'test-result' }
        )

    def test_get_gameresults(self):
        # given
        self.connection.handle_request = MagicMock(return_value=[])

        # when
        self.app.get('/league/test-league/gameresult')

        # then
        self.connection.handle_request.assert_called_with(
            Query.get_gameresults, 'test-league'
        )

    def test_get_game_result_stats(self):
        # given
        self.connection.handle_request = MagicMock(return_value=[])

        # when
        self.app.get('/league/test-league/gameresult/test-result/stats')

        # then
        self.connection.handle_request.assert_called_with(
            Query.get_game_result_stats, 'test-league', 'test-result'
        )

    def test_get_game_stats(self):
        # given
        self.connection.handle_request = MagicMock(return_value=[])

        # when
        self.app.get('/league/test-league/games/stats')

        # then
        self.connection.handle_request.assert_called_with(
            Query.get_game_stats, 'test-league'
        )

    def test_get_player_stats(self):
        # given
        self.connection.handle_request = MagicMock(return_value=[])

        # when
        self.app.get('/league/test-league/players/stats')

        # then
        self.connection.handle_request.assert_called_with(
            Query.get_player_stats, 'test-league'
        )