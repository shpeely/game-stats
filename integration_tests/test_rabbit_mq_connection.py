from integration_tests.rabbit_mg_test import RabbitMQTestCase
from game_stats import main
import unittest

class RabbitMqConnectionTestCase(RabbitMQTestCase):

    def setUp(self):
        super().setUp()
        main.start()

    def tearDown(self):
        super().tearDown()

    @unittest.SkipTest
    def test_get_timeline(self):
        print(self.rpc_call('test1'))
        print(self.rpc_call('test2'))
        print(self.rpc_call('test3'))
        print(self.rpc_call('test4'))
