FROM python:3.7.0

RUN apt-get update -y && apt-get install -y libhdf5-serial-dev

EXPOSE 5000

RUN mkdir /application
WORKDIR /application

ADD . /application

RUN make setup

CMD ["python", "game_stats"]