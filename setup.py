#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of game-stats.
# https://github.com/omnibrain/game-stats

# Licensed under the MIT license:
# http://www.opensource.org/licenses/MIT-license
# Copyright (c) 2016, Raphael Voellmy <r.voellmy@gmail.com>

from setuptools import setup, find_packages
from game_stats import __version__

tests_require = [
    'mock',
    'nose',
    'coverage',
    'yanc',
    'tox',
    'ipdb',
    'coveralls',
    'sphinx',
    'sqlalchemy',
]

setup(
    name='game-stats',
    version=__version__,
    description='Compute Statistics for Game Scores',
    long_description='''
    Compute Statistics for Game Scores
    ''',
    keywords='game scores statistics',
    author='Raphael Voellmy',
    author_email='r.voellmy@gmail.com',
    url='https://github.com/omnibrain/game-stats',
    license='MIT',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Operating System :: Unix',
        'Programming Language :: Python :: 3.5',
        'Operating System :: OS Independent',
    ],
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'preggy==1.4.2',
        'sqlalchemy==1.0.15',
        'tables==3.6.1',
        'pika==0.12.0',
        'pandas==0.25.3',
        'numpy==1.18.0',
        'Flask-API==0.7.1',
        'markdown==2.6.8',
        'boto3==1.9.225',
        'ciso8601==1.0.3'
    ],
    extras_require={
        'tests': tests_require,
    },
    entry_points={
        'console_scripts': [
            # add cli scripts here in this form:
            # 'game-stats=game_stats.cli:main',
        ],
    }
)
